const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const provider = ganache.provider();
const web3 = new Web3(provider);

const compiledDposts = require('../ethereum/build/Dposts.json');
const compiledBoard = require('../ethereum/build/Board.json');

// const { Dposts, Board } = require('../compile');

let accounts;
let dposts;
let lastingDposts;

let firstBoardAddress;
let firstBoard;

before(async () => {
  accounts = await web3.eth.getAccounts();

  // Use one of those accounts to deploy the main Dposts contract
  let gasCost = await new web3.eth.Contract(
    JSON.parse(compiledDposts.interface)
  )
    .deploy({ data: compiledDposts.bytecode, arguments: [] })
    .estimateGas({ from: accounts[0] });

  lastingDposts = await new web3.eth.Contract(
    JSON.parse(compiledDposts.interface)
  )
    .deploy({ data: compiledDposts.bytecode, arguments: [] })
    .send({ from: accounts[0], gas: gasCost });

  console.log('faddress', lastingDposts.options.address);

  lastingDposts.setProvider(provider);
});

beforeEach(async () => {
  // Get a list of all accounts
  accounts = await web3.eth.getAccounts();

  let gasCost = await new web3.eth.Contract(
    JSON.parse(compiledDposts.interface)
  )
    .deploy({ data: compiledDposts.bytecode, arguments: [] })
    .estimateGas({ from: accounts[0] });

  // Use one of those accounts to deploy the main Dposts contract
  dposts = await new web3.eth.Contract(JSON.parse(compiledDposts.interface))
    .deploy({ data: compiledDposts.bytecode, arguments: [] })
    .send({ from: accounts[0], gas: gasCost });

  console.log('faddress', dposts.options.address);

  dposts.setProvider(provider);
});

describe('Dposts', () => {
  it('deploys a contract', () => {
    console.log('daccounts', accounts);
    assert.ok(dposts.options.address);
  });

  it('starts with no initial calculated tax', async () => {
    const lastCurrentTaxCall = await dposts.methods.lastCurrentTax.call();
    const lct = await lastCurrentTaxCall.call();
    console.log('lct', lct);
    assert.equal(lct, 0);
  });
  describe('Board', () => {
    before(async () => {
      await lastingDposts.methods
        .createBoard(
          web3.utils.utf8ToHex('foo'),
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          10
        )
        .send({ from: accounts[1], gas: 6000000 });

      firstBoardAddress = await lastingDposts.methods.boards(0).call();

      firstBoard = await new web3.eth.Contract(
        JSON.parse(compiledBoard.interface),
        firstBoardAddress
      );
    });

    it('allows board creation', () => {
      console.log('fba', firstBoardAddress);
      assert.ok(firstBoardAddress);
    });

    it('gets a working version of a board', () => {
      assert.ok(firstBoard);
    });

    describe('taxed posting', () => {
      let firstPost;

      before(async () => {
        let gasCost = await firstBoard.methods
          .createPost(
            web3.utils.fromAscii('bar1'),
            'bar2',
            web3.utils.fromAscii('www.google.com'),
            10,
            910000000000000
          )
          .estimateGas({ from: accounts[2], value: 910000000000020 });

        await firstBoard.methods
          .createPost(
            web3.utils.fromAscii('bar1'),
            'bar2',
            web3.utils.fromAscii('www.google.com'),
            10,
            910000000000000
          )
          .send({ from: accounts[2], gas: gasCost, value: 910000000000020 });

        firstPost = await firstBoard.methods.posts(0).call();
      });

      it('allows posting', async () => {
        assert.equal(firstPost.body, 'bar2');
      });

      it('reduces tax based on tax pot', async () => {
        const lastCurrentTax = await lastingDposts.methods
          .lastCurrentTax()
          .call();

        assert.ok(lastCurrentTax < 910000000000000);
      });

      it('disallows posting if value is insufficient', async () => {
        const postsStartingLength = await firstBoard.methods
          .getPostsLength()
          .call();

        try {
          await firstBoard.methods
            .createPost(
              'insufficient',
              'bar2',
              'www.google.com',
              10,
              910000000000000
            )
            .send({ from: accounts[2], gas: 6000000, value: 10 });
        } catch (e) {
          // do nothing
        }

        const postsEndingLength = await firstBoard.methods
          .getPostsLength()
          .call();

        assert.equal(postsStartingLength, postsEndingLength);
      });

      describe('commenting', () => {
        before(async () => {
          let gasCost = await firstBoard.methods
            .createComment(0, 'commentbody1')
            .estimateGas({ from: accounts[3] });
          await firstBoard.methods
            .createComment(0, 'commentbody1')
            .send({ from: accounts[3], gas: gasCost });
        });
        it('allows commenting', async () => {
          let commentsLength = await firstBoard.methods
            .getPostCommentsLength(0)
            .call();
          assert.equal(commentsLength, 1);
        });
        it('allows for comment retrieval', async () => {
          let firstCommentIndex = await firstBoard.methods
            .getCommentIndexByPostCommentsIndex(0, 0)
            .call();

          assert.equal(firstCommentIndex, 0);

          let firstComment = await firstBoard.methods
            .comments(firstCommentIndex)
            .call();

          assert.equal(firstComment.body, 'commentbody1');
        });
      });
    });
  });
});
