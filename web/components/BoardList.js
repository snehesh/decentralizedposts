import React, { Component } from 'react';
import BoardCard from './BoardCard';
import web3 from '../../ethereum/web3';
import dposts from '../../ethereum/dposts';
import { Row, Col } from 'reactstrap';

class BoardList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accounts: []
    };

    this.destroyAll = this.destroyAll.bind(this);
  }

  async componentDidMount() {
    const accounts = await web3.eth.getAccounts();
    this.setState({ accounts });
  }

  destroyAll() {
    dposts.methods
      .terminate()
      .send({ from: this.state.accounts[0], gas: 6000000 });
  }

  render() {
    return (
      <Row>
        <Col xs={12} className="board-list" id="boardList">
          {/* <button onClick={this.destroyAll}>destroy all</button> */}
          {this.props.boardIds.map(id => {
            return (
              <BoardCard accounts={this.state.accounts} boardId={id} key={id} />
            );
          })}
          <p className="text-center">
            Any board anyone creates and finishes publishing will appear here!
          </p>
        </Col>
      </Row>
    );
  }
}

export default BoardList;
