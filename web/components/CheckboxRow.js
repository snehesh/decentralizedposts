import React, { Component } from 'react';

import { FormGroup, Label, Col, Input } from 'reactstrap';

class CheckboxRow extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <FormGroup row>
        <Label for={this.props.fieldName} sm={4} lg={3}>
          {this.props.fieldName}
        </Label>
        <Col className="col-checkbox-holder" sm={{ size: 8 }} lg={{ size: 9 }}>
          <FormGroup check>
            <Label check>
              <Input
                type="checkbox"
                onChange={this.props.handleChange}
                checked={this.props.checked}
                id={this.props.fieldName}
                name={this.props.fieldName}
              />
              {this.props.explainer}
            </Label>
          </FormGroup>
        </Col>
      </FormGroup>
    );
  }
}

export default CheckboxRow;
