import React, { Component } from 'react';
import web3 from '../../ethereum/web3';
const compiledBoard = require('../../ethereum/build/Board.json');
import Link from 'next/link';

import { Row, Col, Button, Alert, Table, Card } from 'reactstrap';

class BoardCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      owner: '',
      name: '',
      boardUrl: '',
      urlStr: '',
      owner: '',
      postsLength: 0,
      destroyingBoard: false,
      boardDestroyed: false,
      loadingErrorMessage: '',
      deletionErrorMessage: ''
    };

    this.destroyBoard = this.destroyBoard.bind(this);
  }

  async componentDidMount() {
    const board = await new web3.eth.Contract(
      JSON.parse(compiledBoard.interface),
      this.props.boardId
    );
    let name;
    try {
      // need to block for one accessor first to make sure the board wasn't selfdestructed
      name = await board.methods.name().call();
    } catch (err) {
      this.setState({ loadingErrorMessage: err.message, loading: false });
      return;
    }

    let postPromises = [];

    postPromises.push(board.methods.url().call());
    postPromises.push(board.methods.owner().call());
    postPromises.push(board.methods.getPostsLength().call());

    const postRetrievedValues = await Promise.all(postPromises);

    let urlStr = web3.utils
      .toAscii(postRetrievedValues[0])
      .replace(/\u0000/g, '');

    this.setState({
      name: name,
      boardUrl: postRetrievedValues[0],
      owner: postRetrievedValues[1],
      urlStr: urlStr,
      postsLength: postRetrievedValues[2],
      loading: false
    });
  }

  async destroyBoard() {
    if (!confirm('Are you sure???! This absolutely cannot be undone.')) {
      return;
    }
    try {
      this.setState({
        deletionErrorMessage: '',
        boardDestroyFailed: false,
        destroyingBoard: true
      });
      const board = await new web3.eth.Contract(
        JSON.parse(compiledBoard.interface),
        this.props.boardId
      );
      let gasCost = await board.methods
        .terminate()
        .send({ from: this.props.accounts[0] });

      let terminated = await board.methods
        .terminate()
        .send({ from: this.props.accounts[0], gas: gasCost });

      this.setState({ boardDestroyed: true });

      console.log('terminated', terminated);
    } catch (err) {
      this.setState({
        boardDestroyFailed: true,
        destroyingBoard: false,
        deletionErrorMessage: err.message
      });
    }
  }

  render() {
    const loading = this.state.loading;
    const published = !!this.state.urlStr;
    const currentUserCreated =
      this.props.accounts.length && this.props.accounts[0] === this.state.owner;

    let className = 'board-card';

    if (!published) {
      className += ' unpublished';
    }

    return (
      <Card
        className={className}
        hidden={
          this.state.loadingErrorMessage ||
          (!this.state.loading && !this.state.urlStr && !currentUserCreated)
            ? 'hidden'
            : ''
        }
      >
        <Col className="board-inner-content" xs={12}>
          <h2>{this.state.name && web3.utils.toAscii(this.state.name)}</h2>
          {!published && (
            <p className="unpublished-explainer">
              Board currently unpublished. Add a board URL to publish. See{' '}
              <Link href="/boardhosting" prefetch>
                <a>Board Hosting</a>
              </Link>{' '}
              for more info.
            </p>
          )}
          <Table borderless="true">
            <tbody>
              <tr>
                <th scope="row">Link</th>
                <td>
                  {this.state.urlStr ? (
                    <a
                      target="_blank"
                      rel="noopener noreferrer"
                      href={'http://' + this.state.urlStr}
                    >
                      {this.state.urlStr}
                    </a>
                  ) : (
                    'unpublished'
                  )}
                </td>
              </tr>
              <tr>
                <th scope="row">Contract Address</th>
                <td>
                  <a
                    target="_blank"
                    rel="noopener noreferrer"
                    href={'https://etherscan.io/address/' + this.props.boardId}
                  >
                    {this.props.boardId}
                  </a>
                </td>
              </tr>
              <tr>
                <th scope="row">Owner</th>
                <td>
                  <a
                    target="_blank"
                    rel="noopener noreferrer"
                    href={'https://etherscan.io/address/' + this.state.owner}
                  >
                    {this.state.owner}
                  </a>
                </td>
              </tr>
              <tr>
                <th scope="row">Post Count</th>
                <td>{this.state.postsLength}</td>
              </tr>
            </tbody>
          </Table>

          {this.state.deletionErrorMessage ? (
            <Alert color="danger">
              {this.state.deletionErrorMessage.substring(0, 120)}
            </Alert>
          ) : (
            ''
          )}
          {loading ? (
            <div>loading...</div>
          ) : (
            <div>
              {this.state.boardDestroyed ? (
                <p style={{ color: 'red' }}>Board Destroyed</p>
              ) : (
                ''
              )}

              {currentUserCreated ? (
                <div className="destroy-button-holder">
                  <p>You are the owner of this board.</p>
                  <Button
                    color="danger"
                    disabled={this.state.destroyingBoard}
                    onClick={this.destroyBoard}
                  >
                    {this.state.destroyingBoard ? 'Destroying...' : 'Destroy'}
                  </Button>
                </div>
              ) : null}
              {this.state.destroyingBoard ? (
                <p style={{ color: 'red' }}>This will take a few minutes...</p>
              ) : (
                ''
              )}
            </div>
          )}
        </Col>
      </Card>
    );
  }
}

export default BoardCard;
