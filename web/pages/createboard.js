import React from 'react';
import Link from 'next/link';
import Page from '../components/Page';
import BoardCreation from '../components/BoardCreation';

class CreateBoard extends React.Component {
  render() {
    return (
      <Page>
        <BoardCreation />
      </Page>
    );
  }
}

export default CreateBoard;
