// import '../main-styles.css';
import { Button, Jumbotron, Alert } from 'reactstrap';
import Head from 'next/head';
import dposts from '../../ethereum/dposts';
import React from 'react';
import Page from '../components/Page';
import BoardCreation from '../components/BoardCreation';
import BoardList from '../components/BoardList';

import { GA_KEY } from '../../secrets.json';

class Index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      boardCount: 0,
      boardIds: []
    };
  }

  async componentDidMount() {
    const boardCount = await dposts.methods.getBoardsLength().call();
    this.setState({ boardCount });
    const boardPromises = [];

    for (let i = 0; i < boardCount; i++) {
      boardPromises.push(dposts.methods.boards(i).call());
    }

    const boardIds = await Promise.all(boardPromises);
    this.setState({ boardIds });
  }

  render() {
    return (
      <Page>
        {process.env.NODE_ENV === 'production' && (
          <React.Fragment>
            <Head>
              <script
                dangerouslySetInnerHTML={{
                  __html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','${GA_KEY}');`
                }}
              />
            </Head>
            <noscript
              dangerouslySetInnerHTML={{
                __html: `<iframe src="https://www.googletagmanager.com/ns.html?id=${GA_KEY}" height="0" width="0" style="display:none;visibility:hidden;"></iframe>`
              }}
            />
          </React.Fragment>
        )}

        <Jumbotron>
          <h1 className="display-3">Decentralized Posts</h1>
          <p className="lead">
            Welcome to the Federated Network of Decentralized Posts!
          </p>
          <a href="#boardList">
            <Button color="primary">Show me the boards</Button>
          </a>
        </Jumbotron>
        <BoardList boardIds={this.state.boardIds} />
        <div
          style={{
            width: '100%',
            textAlign: 'left'
          }}
          id="about"
        >
          <div
            style={{
              textAlign: 'left',
              maxWidth: 600,
              margin: '16px auto'
            }}
          >
            <h1>About:</h1>
            <p>
              This is a network of simple text posting boards on the Ethereum
              blockchain.
            </p>
            <p>
              This was a fun experimental side project I built mostly to learn.
              The experience was interesting, so I want to share my thoughts
              here.
            </p>
            <p>
              I was interested in decentralized apps and cryptocurrencies, but I
              wanted to find out what potential there really is in the space for someone like myself.
            </p>
            <p>For me the best (and most fun) way to learn is to build!</p>
            <p>
              These decentralized app companies need to hire, so I decided to
              build a dead simple job board just for them. The existing ones
              seemed too complex. I wanted to make something simple like{' '}
              <a
                href="https://remoteok.io/"
                target="_blank"
                rel="noopener noreferrer"
              >
                remoteok.io
              </a>
              , but on Ethereum.
            </p>
            <p>
              (This now exists at{' '}
              <a
                href="https://www.decentralizedjobs.com/"
                target="_blank"
                rel="noopener noreferrer"
              >
                decentralizedjobs.com
              </a>!)
            </p>
            <p>
              I priced it at .25 ETH per post. If I sell a handful, I paid for
              the dev time. If I sell none... oh well I still had fun and
              learned :D
            </p>
            <p>
              Quicky I saw when I started building what it *feels* like to build
              a decentralized app given the current limitations of the Ethereum
              platform.
            </p>
            <p>
              Counterintuitively (and this is my main point), writing an app
              that uses decentralized infrastructure makes you want to build the
              most centralized app EVER.
            </p>
            <p>What? No, a dapp is decentralized, right?</p>
            <h2>Writing a Dapp</h2>
            <p>
              You're really hamstrung by the platform, but you also can kind of
              just write whatever rules you want. You often want to make yourself the
              dictator, have all the money go to you, and put yourself in charge
              of all the moderation. Almost every single dapp in existence today
              works exactly like this.
            </p>
            <p>
              That's the opposite of why I was interested in this. I was only
              drawn to dapps as a possible alternative to the way technology
              works today. We have all these silos... Google, Facebook, Apple,
              Microsoft, Amazon, etc. and and none of them work together.  I was hoping dapps would be a new tool in our belt to help us work
              together instead of against each other.
            </p>
            <p>
              How do I make this open? How do I work within the constraints of
              this system to build something more in line with my values?
            </p>
            <p>
              It was when I started to explore the possibilities in this vein
              that I realized more what writing a decentralized app really feels
              like.
            </p>
            <p>
              Writing a decentralized app feels like each line of code you write
              is a moral decision.
            </p>
            <p>You can't change the contract once it's live. You can't patch.</p>
            <p>
              You can't be Instagram and say, "Hey, everybody can see
              everybody's pictures here!" and then the next day decide you'll make more profit if none of the posts are in order anymore and if you want people to
              see them people have to pay you and if people don't like it... well it's closed so nobody else can make an Instagram client. You just write your app, and the decisions you make writing it are
              how it works. That makes the decisions you make as you build feel like bigger decisions.
            </p>
            <p>
              Who gets the money? What's the incentive structure? Who governs
              it? What rights do users have? How portable is the data? How
              extensible is it? What rules are in place? How is it moderated?
            </p>
            <p>
              This is why so many people get so religious about what blockchain
              and dapps are and aren't. Dapps are a mirror. Everybody looks at the opportunities presented here and sees the
              ability to realize and enforce how they think things should work. I've seen people with *extremely* different political and moral
              views from my own express that dapps are obviously going to
              finally unlock the world to work in the way that they want. I looked at it and for me it felt like they were going to bring
              about the exact opposite.
            </p>
            <p>
              When I looked at this and asked, is this a way to make it so the
              future of technology isn't just more Facebooks? I obviously found
              shades of an answer of yes because *it's a mirror*. If the way
              something like Facebook operates goes against your morality, then
              yes, you can write an application that embodies different values.
            </p>
            <p>And that's what I did.</p>
            <h2>So...</h2>
            <p>So welcome to the Federated Network of Decentralized Posts!</p>
            <p>
              In the Federated Network of Decentralized Posts, everybody can
              have their own board with its own rules and be in charge. There is
              a root platform all the boards live on, but you're more
              incentivized to start a board within the existing platform than to
              start a whole new platform yourself.
            </p>
            <p>
              That's because the platform has a hard cap of how much money it
              can make, while boards within the platform can make unlimited
              money (through paid posting or tips). I set the platform cap at
              100 ETH/year, and this increases with compound interest each year.
              I picked that amount because it was round and kind of similar in
              value at the time to the average American income per year. It
              would be like enough to pay a single maintainer a reasonable
              salary.
            </p>
            <p>
              When users post on any board they can pay a voluntary tax of a few
              cents to the platform. The closer the pot gets to the cap, the
              lower the tax gets.
            </p>
            <p>
              Individuals who post on boards also have uncapped income. They can
              make unlimited money through tips.
            </p>
            <p>
              Anybody can pull the posts from anybody else's board like an API.
            </p>
            <p>
              Is this perfect? No! The value of ETH fluctuates. The visual
              design and code of this are pretty funny how bad they are. I
              didn't trust the code or structure of it enough to not make myself
              the contract owner (I'd be surprised if the contract is bug-free).
              This is just a sketch of what I think is a new possible
              combination of a bunch of old ideas that are cool. I'm not so sure
              this precise combination (federation on top of decentralization
              with a constrained incentive structure) was possible before the
              advent of dapps, but it's an interesting combo worth exploring.
            </p>
            <p>
              I'm also not saying this is how things should work. Most people
              building dapps today would probably think a lot of how I made this
              work is silly. Actually storing the posts on the blockchain alone
              is probably impossibly impractical in most people's eyes (it makes
              posting super expensive). I just didn't want to compromise on the
              decentralization or fall back to ipfs. This is what a 100% raw
              Ethereum platform app looks like and costs.
            </p>
            <p>
              With federation on top of centralization like{' '}
              <a
                href="https://joinmastodon.org/"
                target="_blank"
                rel="noopener noreferrer"
              >
                Mastodon
              </a>{' '}
              or email, if the server you're using disappears or does something
              you don't agree with... well tough luck. In contrast, federation
              on top of decentralization has some interesting possibilities.
            </p>
            <h2>Going Forward</h2>
            <p>
              I don't think the time for mass use of something like this is
              anywhere near yet (a decade away if ever?), but there are dangers
              here to be considering as long as we're playing with fire.
            </p>
            <p>
              What if everything costs money, so poor people can't participate
              at all and lose a voice completely?
            </p>
            <p>
              What if the rules that are so unchangeable actually are harmful to
              users and can't be fixed?
            </p>
            <p>
              What if the decentralized network is recentralized and people's
              trust comes back to bite them?
            </p>
            <p>
              What if we never find a way to effectively moderate content, and
              it becomes a cryptographically uncleanable cesspool?
            </p>
            <p>
              What if the clients aren't trustworthy and they start doing bad
              things outside of the architecture of the dapp?
            </p>
            <p>
              What if the incentive systems stay strong and we never go away
              from proof of work and Ethereum/Bitcoin keep wasting obscene
              amounts of energy?
            </p>
            <p>
              What if we can't think now of the most dangerous thing about all
              this?
            </p>
            <p>
              Regardless, I just think it's time to start thinking about the
              bigger picture of the apps you choose to use and build and decide
              for yourself what aligns with your values. Technology is crazy
              powerful, and you can make a meaningful vote with your presence
              (or lack thereof).
            </p>
            <p>
              And this was the only way to build my simple job board that felt
              like it was in keeping with the spirit of what sparked my interest
              :D
            </p>
            <p>
              Similar to how what I built is a sketch, after building on Ethereum I see
              that it is itself only a rough sketch bigger ideas of what kind of
              platform could be possible in the future. Can't wait to see what
              lies ahead.
            </p>
            <p>Happy posting, happy building!</p>
          </div>
        </div>
      </Page>
    );
  }
}

export default Index;
