import React from 'react';
import Link from 'next/link';
import Page from '../components/Page';
import dposts from '../../ethereum/dposts';
import { Row, Col } from 'reactstrap';

class Platform extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lastCurrentTax: 0,
      owner: '',
      address: '',
      boardCount: 0,
      loading: true
    };
  }

  async componentDidMount() {
    const boardCount = dposts.methods.getBoardsLength().call();
    const owner = dposts.methods.owner().call();
    const lastCurrentTax = dposts.methods.lastCurrentTax().call();
    const address = dposts.options.address;

    this.setState({
      boardCount: await boardCount,
      owner: await owner,
      lastCurrentTax: parseInt(await lastCurrentTax),
      loading: false,
      address
    });
  }

  render() {
    return (
      <Page>
        {this.state.loading ? (
          <img src="https://loading.io/spinners/bars/index.progress-bar-facebook-loader.svg" />
        ) : (
          <Row>
            <Col>
              <h1>Decentralized posts platform information</h1>
              <p>
                Platform contract address:{' '}
                <a
                  href={'https://etherscan.io/address/' + this.state.address}
                  target="_blank"
                >
                  {this.state.address}
                </a>
              </p>
              <p>
                Platform owner address:{' '}
                <a
                  href={'https://etherscan.io/address/' + this.state.owner}
                  target="_blank"
                >
                  {this.state.owner}
                </a>
              </p>

              <p>Board count: {this.state.boardCount}</p>
              <p>
                Current voluntary tax per post:{' '}
                {this.state.lastCurrentTax === 0
                  ? 'Will be initialized for the current year when the first person pays tax while posting'
                  : '{this.state.lastCurrentTax}' + '(wei)'}
              </p>
            </Col>
          </Row>
        )}
      </Page>
    );
  }
}

export default Platform;
