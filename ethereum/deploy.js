const HDWalletProvider = require('truffle-hdwallet-provider');
const Web3 = require('web3');
const compiledDposts = require('./build/Dposts.json');
const WALLET_KEY = require('../wallet-key.json').WALLET_KEY;
const INFURA_RINKEBY = require('../secrets.json').INFURA2.RINKEBY;
const INFURA_MAINNET = require('../secrets.json').INFURA2.MAINNET;

const provider = new HDWalletProvider(WALLET_KEY, INFURA_MAINNET, 1, 1);
const web3 = new Web3(provider);

const deploy = async () => {
  const accounts = await web3.eth.getAccounts();

  console.log('Attempting to deploy from account', accounts[0]);

  try {
    const result = await new web3.eth.Contract(
      JSON.parse(compiledDposts.interface)
    )
      .deploy({ data: compiledDposts.bytecode })
      .send({ gas: 6000000, from: accounts[0] });

    console.log('Contract deployed to', result.options.address);
  } catch (err) {
    console.error('deploy failed with error', err, err.message);
  }
};
deploy();
